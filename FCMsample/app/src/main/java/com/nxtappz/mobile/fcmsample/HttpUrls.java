package com.nxtappz.mobile.fcmsample;

import okhttp3.HttpUrl;

/**
 * Created by chathura on 7/8/17.
 */

class HttpUrls {

    private static String baseUrl ="http://192.168.43.254/fcmsample/";



    public static String getRegisterUrl() {
        return baseUrl + "register.php";
    }


    public static String countUrl(String count){
        return baseUrl + "count.php?value=" +  count;
    }

}
