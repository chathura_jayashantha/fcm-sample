package com.nxtappz.mobile.fcmsample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends Activity {


    private String value = "0";
    private TextView tvCount;

    static boolean active = false;

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }



    private BroadcastReceiver mBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            value = intent.getStringExtra("value");
            updateTextView();
        }
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e("starting", "================================");

        FirebaseMessaging.getInstance().subscribeToTopic("test");
        FirebaseInstanceId.getInstance().getToken();


        tvCount = (TextView) findViewById(R.id.tvCount);
        tvCount.setText(value);

        if(getIntent().hasExtra("value")){
            value = getIntent().getStringExtra("value");
            updateTextView();
        }


        registerReceiver( mBroadCastReceiver, new IntentFilter("FCM_Counter_sample"));
    }


    public void onClickCountUp(View view) {
        OkHttpClient client = new OkHttpClient();
        value = getIncrementedValue(value);
        RequestBody body = new FormBody.Builder()
                .add("value", value)
                .build();

        Request request = new Request.Builder()
                .url(HttpUrls.countUrl(value))
                .get()
                .build();

        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e("Error", e.getLocalizedMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.w("response ----- ", response.body().string());
                    //tvCount.setText(value);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getIncrementedValue(String value) {
        int iValue = Integer.parseInt(value);
        iValue++;
        return "" + iValue;
    }



    public void updateTextView() {
        tvCount.setText(value);
    }
}
