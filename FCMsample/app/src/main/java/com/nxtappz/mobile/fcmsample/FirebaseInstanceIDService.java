package com.nxtappz.mobile.fcmsample;

import android.app.Service;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by chathura on 7/8/17.
 */

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e("called " , "-------------------------");
        registerToken(token);
    }

    private void registerToken(String token) {

        Log.w("Token -------- " , token);

        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("Token",token)
                .build();

        Request request = new Request.Builder()
                .url(HttpUrls.getRegisterUrl())
                .post(body)
                .build();

        try {
            Response response =  client.newCall(request).execute();
            Log.w("response ----- " ,  response.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
